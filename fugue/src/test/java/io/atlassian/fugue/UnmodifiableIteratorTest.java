package io.atlassian.fugue;

import java.util.Iterator;

import org.junit.Test;

public class UnmodifiableIteratorTest {

    @Test(expected = UnsupportedOperationException.class)
    public void testRemove() {
        final Iterators.Unmodifiable<Integer> unmodifiableIterator = new Iterators.Unmodifiable<Integer>() {
            @Override
            public boolean hasNext() {
                return false;
            }

            @Override
            public Integer next() {
                return null;
            }
        };

        remove(unmodifiableIterator);
    }

    @SuppressWarnings("deprecation")
    private void remove(final Iterator<Integer> i) {
        i.remove();
    }
}
