package io.atlassian.fugue;

import org.junit.Test;

import static io.atlassian.fugue.Iterables.count;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.junit.Assert.assertEquals;

public class IterablesCountTest {

    @Test
    public void countOnlyEvenNumbers() {
        final Iterable<Integer> from = asList(1, 2, 3, 4, 5, 6);

        final int result = count(from, i -> i % 2 == 0);

        assertEquals(3, result);
    }

    @Test
    public void countOnlyStringStartedWithB() {
        final Iterable<String> from = asList("Bamboo", "Jira", "bamboo", "BBS", "Confluence", "jira");

        final int result = count(from, s -> s.toLowerCase().startsWith("b"));

        assertEquals(3, result);
    }

    @Test
    public void countSuccessfullyWithNestedTypes() {
        final Iterable<Pair<Long, String>> from = asList(
                Pair.pair(1L, "Bamboo"),
                Pair.pair(4L, "Jira"),
                Pair.pair(6L, "bamboo"),
                Pair.pair(4L, "BBS"),
                Pair.pair(13L, "Confluence"),
                Pair.pair(1L, "jira"));

        final int result = count(from, p -> p.left() == p.right().length());

        assertEquals(2, result);
    }

    @Test
    public void emptyIterableEndsUpWithZeroResult() {
        final Iterable<Integer> from = emptyList();

        final int result = count(from, _unused -> true);

        assertEquals(0, result);
    }

    @Test(expected = IterablesCountTest.JavaCollectorRuntimeException.class)
    public void foldLeftThrowsRuntimeException() {
        final Iterable<String> from = asList("3", "2", "1", "4", "5");
        count(from, _unused -> {
            throw new IterablesCountTest.JavaCollectorRuntimeException();
        });
    }

    @Test(expected = NullPointerException.class)
    @SuppressWarnings("unchecked")
    public void nullFoldLeftMustResultInNPE() {
        final Iterable<String> from = asList("3", "2", "1", "4", "5");
        count(from, null);
    }

    private static class JavaCollectorRuntimeException extends RuntimeException {}
}
