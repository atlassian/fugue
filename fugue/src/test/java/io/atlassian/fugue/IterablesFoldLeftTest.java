package io.atlassian.fugue;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;

import static io.atlassian.fugue.Iterables.foldLeft;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class IterablesFoldLeftTest {

    @Test
    public void sumTheListOfIntegers() {
        final Iterable<Integer> from = asList(1, 2, 3, 4, 5);

        final Integer result = foldLeft(from, Integer::sum, 0);

        assertNotNull(result);
        assertEquals((Integer) 15, result);
    }

    @Test
    public void createStringFromListOfIntegers() {
        final Iterable<Integer> from = asList(1, 2, 3, 4, 5);

        final String result = foldLeft(from, (prevString, item) -> prevString + "/" + item, "START");

        assertEquals("START/1/2/3/4/5", result);
    }

    @Test
    public void transformListIntoMap() {
        final Iterable<Long> from = asList(1L, 2L, 3L, 4L, 5L, 1L, 3L, 4L, 1L, 2L, 4L);

        final Map<Long, Integer> result = foldLeft(
                from,
                (prevMap, item) -> {
                    prevMap.compute(item, (_unused, prevValue) -> prevValue == null ? 1 : prevValue + 1);
                    return prevMap;
                }, // Of course, a mutable map does not make much sense here
                new HashMap<>());

        final Map<Long, Integer> expected = Stream.of(
                        new AbstractMap.SimpleEntry<>(1L, 3),
                        new AbstractMap.SimpleEntry<>(2L, 2),
                        new AbstractMap.SimpleEntry<>(3L, 2),
                        new AbstractMap.SimpleEntry<>(4L, 3),
                        new AbstractMap.SimpleEntry<>(5L, 1))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        assertNotNull(result);
        assertEquals(expected, result);
    }

    @Test
    public void returnWithNoDuplicates() {
        final Iterable<String> from = asList("bamboo", "jira", "bbs", "bbc", "bbs", "bamboo", "jira", "jira");

        final Iterable<String> result = foldLeft(
                from,
                (noDuplicatesList, item) -> {
                    if (!noDuplicatesList.contains(item)) {
                        noDuplicatesList.add(item);
                    }
                    return noDuplicatesList;
                },
                new ArrayList<>());

        final Iterable<String> expected = asList("bamboo", "jira", "bbs", "bbc");

        assertEquals(expected, result);
    }

    @Test
    public void emptyIterableEndsUpComputationWithInitialValue() {
        final Iterable<Integer> from = emptyList();

        final Integer result = foldLeft(from, Integer::sum, 144);

        assertNotNull(result);
        assertEquals((Integer) 144, result);
    }

    @Test(expected = IterablesFoldLeftTest.JavaCollectorRuntimeException.class)
    public void foldLeftThrowsRuntimeException() {
        final Iterable<String> from = asList("3", "2", "1", "4", "5");

        foldLeft(
                from,
                (prev, current) -> {
                    throw new IterablesFoldLeftTest.JavaCollectorRuntimeException();
                },
                144);
    }

    @Test(expected = NullPointerException.class)
    @SuppressWarnings("unchecked")
    public void nullFoldLeftMustResultInNPE() {
        final Iterable<String> from = asList("3", "2", "1", "4", "5");

        foldLeft(from, null, 0);
    }

    private static class JavaCollectorRuntimeException extends RuntimeException {}
}
